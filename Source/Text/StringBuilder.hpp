/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */
#pragma once

#include <iterator>
#include <string>

namespace Text {

    class StringBuilder {
        std::string data{};
    public:
        inline constexpr void
        Reserve(std::size_t size) noexcept {
            data.reserve(size);
        }

        //
        // Append Functions
        //

        inline StringBuilder &
        Append(const std::string &string) {
            data += string;
            return *this;
        }

        inline StringBuilder &
        Append(char character) {
            data += character;
            return *this;
        }

        //
        // Iterator Support
        //
        [[nodiscard]] inline auto
        begin() noexcept -> decltype(std::begin(data)) {
            return data.begin();
        }

        [[nodiscard]] inline auto
        end() noexcept {
            return data.end();
        }

        [[nodiscard]] inline auto
        cbegin() const noexcept {
            return data.cbegin();
        }

        [[nodiscard]] inline auto
        cend() const noexcept {
            return data.cend();
        }

        [[nodiscard]] inline auto
        crbegin() const noexcept {
            return data.crbegin();
        }

        [[nodiscard]] inline auto
        crend() const noexcept {
            return data.crend();
        }

        [[nodiscard]] inline auto
        rbegin() noexcept {
            return data.rbegin();
        }

        [[nodiscard]] inline auto
        rend() noexcept {
            return data.rend();
        }

    };


} // namespace Text
