/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <iostream>

#include "Source/Base/Configuration.hpp"
#include "Source/Base/ExitCodes.hpp"
#include "Source/Base/SignalHandler.hpp"
#include "Source/HTTP/Client.hpp"
#include "Source/HTTP/RequestReader.hpp"
#include "Source/Resources/ServerSocket.hpp"
#include "Source/Resources/ThreadManager.hpp"
#include "Source/Security/TLSManager.hpp"

bool flag{true};

void
Handler(const Base::Configuration &configuration,
        std::shared_ptr<Resources::Connection> &&connection) {
    try {
        HTTP::Client client(configuration, connection);
        client.Run();
    } catch (const HTTP::RequestReader::ReaderException &exception) {
        std::cout << "HTTP ReaderException in thread: \"" << exception.what() << "\"!\n";
    } catch (const std::exception &exception) {
        std::cout << "Exception in thread: \"" << exception.what() << "\"!\n";
    }

    // if we don't call std::shared_ptr<T>::reset() the object doesn't get
    // deleted which results in a memory leak.
    // I've debugged this for 4 hours and can't find to seem why. At the end of
    // the function the std::shared_ptr<>::use_count() reports 1, but the
    // destructor isn't called, and valgrind detects a memory leak. very weird.
    connection.reset();
}

int main() {
    Base::SignalHandler signalHandler{};

    const Base::Configuration configuration{};

    Security::TLSManager manager(configuration);

    Resources::ServerSocket socket{
            configuration.serverSocketConfiguration.port,
            configuration.serverSocketConfiguration.backlog
    };

    signalHandler.RegisterHook([&socket]() mutable {
        socket.Shutdown();
    });

    std::cout << "Successfully created socket on port: "
              << configuration.serverSocketConfiguration.port << "\n";

    Resources::ThreadManager threadManager{
        configuration.threadManagerConfiguration.threadCount,
        configuration.threadManagerConfiguration.maximumQueuedJobs
    };

    while (flag) {
        try {
            threadManager.WaitReady();

            auto connection = socket.AcceptTLS(&manager);

            threadManager.ScheduleJob([&configuration, connection{std::move(connection)}]() mutable {
                Handler(configuration, std::move(connection));
            });
        } catch (const Resources::ServerSocketException &exception) {
            if (signalHandler.WasFired()) {
                // Signal was caught which results in this exception, thus we
                // need to take the success path.
                break;
            }
            if (exception.isServerLevelFatal) {
                std::cerr << "Server-level fatal ServerSocketException encountered: " << exception.what() << '\n';
                return Base::ExitCodes::ServerSocketFailure;
            }
        } catch (const Security::TLSManagerException &) {
            // TODO it would be wise to report this exception in the future, if
            //      some verbose flag was toggled in the configuration.
            //      syslog(3) might be interesting
        }
    }


    //
    // To make valgrinds --track-fds happy, close parent fds 0, 1 and 2.
    //

    std::fclose(stdout);
    std::fclose(stdin);
    std::fclose(stderr);

    return Base::ExitCodes::Success;
}
