/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <cstdint>

namespace Base {

    struct Configuration {

        /**
         * This struct defines limits for the reading the request. Any std::size_t
         * with value of 0 is unlimited.
         */
        struct RequestLimits {

            std::size_t maximumRequestLineTotalLength{8000};
            std::size_t maximumMethodLength{50};
            std::size_t maximumRequestTargetLength{300};

        };

        struct ServerSocketConfiguration {

            std::uint16_t port{8080};
            std::uint16_t backlog{50};

        };

        struct ThreadManagerConfiguration {

            std::size_t maximumQueuedJobs{50};

            // std::thread::hardware_concurrency() ?
            std::size_t threadCount{12};

        };

        struct TLSConfiguration {

            std::string certificateFile{"/usr/local/etc/certs/server.crt"};
            std::string privateKeyFile{"/usr/local/etc/certs/server.key"};
            std::string certificateChainFile{"/usr/local/etc/certs/fullchain.pem"};

        };

        RequestLimits requestLimits{};
        ServerSocketConfiguration serverSocketConfiguration{};
        ThreadManagerConfiguration threadManagerConfiguration{};
        TLSConfiguration tlsConfiguration{};

    };

} // namespace Base
