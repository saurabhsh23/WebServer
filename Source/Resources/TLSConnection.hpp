/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <exception>
#include <type_traits>
#include <vector>

#include "Source/Resources/Connection.hpp"
#include "Source/Resources/SocketSSLGlue.hpp"

namespace Resources {

    class TLSConnection : public Connection {
    private:
        Resources::SocketSSLGlue::TLSObjectType data;
        Resources::SocketSSLGlue::SocketType socketInfo;

    public:
        class TLSConnectionException : public std::exception {
        private:
            const char *const info;

        public:
            explicit
            TLSConnectionException(const char *info)
                    : info(info) {
            }

            [[nodiscard]] const char *
            what() const noexcept override {
                return info;
            }
        };

        constexpr
        TLSConnection(Resources::SocketSSLGlue::TLSObjectType data,
                      Resources::SocketSSLGlue::SocketType socketInfo)
                requires(std::is_trivially_copyable_v<Resources::SocketSSLGlue::TLSObjectType>)
                : data(data), socketInfo(socketInfo) {
        }

        constexpr
        TLSConnection(Resources::SocketSSLGlue::TLSObjectType &&data,
                      Resources::SocketSSLGlue::SocketType socketInfo)
                requires(!std::is_trivially_copyable_v<Resources::SocketSSLGlue::TLSObjectType>)
                : data(std::move(data)), socketInfo(socketInfo) {
        }

        ~TLSConnection() noexcept override;

        [[nodiscard]] char
        ReadChar() override;

        void
        ReadBlock(char *data, std::size_t length) override;

        void
        Write(const char *data, std::size_t dataLength) override;
    };

} // namespace Connection
