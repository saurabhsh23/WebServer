/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "ThreadManager.hpp"

namespace Resources {

    void ThreadManager::ThreadEntrypoint() {
        ThreadJob job;

        while (true) {
            {
                std::unique_lock<std::mutex> lock(jobQueueMutex);

                condition.wait(lock, [this] { return !jobs.empty() || terminateFlag; });

                if (terminateFlag) {
                    return;
                }

                job = jobs.front();
                jobs.pop();
            }

            job.entrypoint();
        }
    }

    void
    ThreadManager::WaitReady() {
        while (jobs.size() >= maximumQueuedJobs) {}
    }

    ThreadManager::~ThreadManager() noexcept {
        {
            std::unique_lock<std::mutex> lock(jobQueueMutex);
            terminateFlag = true;
        }

        condition.notify_all(); // wake up all threads.

        // Join all threads.
        for (auto &thread : pool) {
            thread.join();
        }

        pool.clear();
    }

} // namespace Resources
