/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <exception>
#include <vector>

#include "Connection.hpp"

namespace Resources {

    class MockConnection : public Connection {
    private:
        const std::vector<char> input{};
        std::vector<char> output{};

        std::vector<char>::const_iterator inputIt;

    public:
        class MockConnectionException : public std::exception {
        private:
            const char *const info;

        public:
            explicit
            MockConnectionException(const char *info)
                : info(info) {
            }

            [[nodiscard]] const char *
            what() const noexcept override {
                return info;
            }
        };

        explicit
        MockConnection(std::vector<char> &&input)
            : input(std::move(input)), inputIt(std::cbegin(this->input)) {
        }


        [[nodiscard]] char
        ReadChar() override {
            if (inputIt >= std::end(input))
                throw MockConnectionException("eof");
            return *(inputIt++);
        }

        void
        ReadBlock(char *data, std::size_t length) override {
            if (inputIt + length >= std::end(input))
                throw MockConnectionException("not enough data to ReadBlock");
            std::copy(inputIt, inputIt + length, data);
            inputIt += length;
        }

        void
        Write(const char *data, std::size_t dataLength) override {
            // TODO pointer arithmetic
            output.insert(std::end(output), data, data + dataLength);
        }
    };

} // namespace Connection
