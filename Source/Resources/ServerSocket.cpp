/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "ServerSocket.hpp"

#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))
#include "Platform/POSIX/Resources/ServerSocket.cpp"
#else
    #error "Platform not supported!"
#endif
