/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <exception>
#include <vector>

#include <unistd.h>

#include "Connection.hpp"

namespace Resources {

    class SocketConnection : public Connection {
    private:
        int sockfd;

    public:
        class SocketConnectionException : public std::exception {
        private:
            const char *const info;

        public:
            explicit
            SocketConnectionException(const char *info)
                    : info(info) {
            }

            [[nodiscard]] const char *
            what() const noexcept override {
                return info;
            }
        };

        constexpr explicit
        SocketConnection(int sockfd) : sockfd(sockfd) {}

        inline ~SocketConnection() noexcept override {
            Resources::SocketSSLGlue::DestroySocketInfo(sockfd);
        }

        [[nodiscard]] char
        ReadChar() override {
            char character;
            ssize_t ret = read(sockfd, &character, 1);

            if (ret != 1)
                throw SocketConnectionException("eof in read char");
            return character;
        }

        void
        ReadBlock(char *data, std::size_t length) override {
            do {
                ssize_t ret = read(sockfd, data, length);
                if (ret == -1)
                    throw SocketConnectionException("error in read block");
                if (ret == 0)
                    throw SocketConnectionException("eof in read block");

                data += ret;
                length -= ret;
            } while (length > 0);
        }

        void
        Write(const char *data, std::size_t dataLength) override {
            do {
                ssize_t ret = write(sockfd, data, dataLength);
                if (ret <= 0)
                    throw SocketConnectionException("error in write");

                data += ret;
                dataLength -= ret;
            } while (dataLength > 0);
        }
    };

} // namespace Connection
