/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace Resources {

    /**
     * Abstract interface for connections.
     *
     * Made abstract to support:
     * - raw platform-dependent streams (e.g. UNIX file descriptors)
     * - TLS-wrapped streams
     * - Mock streams for testing.
     *
     * Currently the interfaces use raw pointers for passsing data, but in the
     * future we might want to port this to use
     */
    class Connection {
    public:
        //
        // Abstract interface functions to be implemented by the derived class.
        //

        virtual ~Connection() = default;

        [[nodiscard]] virtual char
        ReadChar() = 0;

        virtual void
        ReadBlock(char *, std::size_t) = 0;

        virtual void
        Write(const char *, std::size_t) = 0;

        //
        // Template functions for ease and errorless use of the interface
        // functions.
        //

        template<typename T>
        void
        ReadBlock(T &result) {
            ReadBlock(std::begin(result), std::distance(std::begin(result), std::end(result)));
        }

        template<typename T>
        void
        Write(const T &value) {
            Write(&(*std::cbegin(value)), std::distance(std::cbegin(value), std::cend(value)));
        }

        template<typename T>
        void
        Write(T &value) {
            Write(&(*std::begin(value)), std::distance(std::begin(value), std::end(value)));
        }
    };

} // namespace Resources
