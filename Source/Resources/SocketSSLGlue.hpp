/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * This glue class provides
 */
#pragma once

#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))
    #include <unistd.h>
#endif

#ifdef TLS_USE_OPENSSL
    #include <openssl/ssl.h>
#endif

namespace Resources::SocketSSLGlue {

#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))

    using SocketType = int;

    inline static void
    DestroySocketInfo(Resources::SocketSSLGlue::SocketType socketInfo) {
        close(socketInfo);
    }

#else
    #error "Platform not supported!"
#endif

#ifdef TLS_USE_OPENSSL

    using TLSObjectType = SSL *;

#else
    #error "No TLS library specified"
#endif

} // namespace Resources::SocketSSLGlue