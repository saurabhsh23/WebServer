/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */
#pragma once

#include <cstdint>

#include <exception>
#include <memory>
#include <string>

#include "Source/Resources/Connection.hpp"
#include "Source/Security/TLSManager.hpp"

namespace Resources {

    struct ServerSocketException : public std::exception {

        explicit
        ServerSocketException(const char *c_string, bool isServerLevelFatal=false)
            : info(c_string), isServerLevelFatal(isServerLevelFatal) {}

        explicit
        ServerSocketException(std::string &&string, bool isServerLevelFatal=false)
                : info(std::move(string)), isServerLevelFatal(isServerLevelFatal) {}

        explicit
        ServerSocketException(const std::string &string, bool isServerLevelFatal=false)
                : info(string), isServerLevelFatal(isServerLevelFatal) {}

        std::string info;

        /**
         * Indicates whether or not the exception is fatal for the server
         * socket as a whole (e.g. implementation is faulty, OS closed socket,
         * etc.), or not.
         *
         * Client-level fatal would be the client closing the connection before
         * handshake, or something alike.
         */
        bool isServerLevelFatal{false};

        [[nodiscard]] const char *
        what() const noexcept override {
            return info.c_str();
        }

        friend std::ostream &
        operator<<(std::ostream &stream, const ServerSocketException &exception) {
            stream << exception.info;
            return stream;
        }

    };

    class ServerSocket {
    public:
        ServerSocket(std::uint16_t port, int listenerBacklog);

        ServerSocket(const ServerSocket &) = delete;

        ~ServerSocket() noexcept;

        [[nodiscard]] std::shared_ptr<Connection>
        Accept();

        [[nodiscard]] std::shared_ptr<Connection>
        AcceptTLS(Security::TLSManager *);

        /**
         * Shutdown the socket manually (i.e. non-destruct), if it isn't
         * already shutdown.
         */
        void Shutdown() noexcept;

    private:
#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))
        volatile int sockfd{};
#else
#error "Platform not supported!"
#endif

        [[nodiscard]] SocketSSLGlue::SocketType
        AcceptSocket();
    };

}
