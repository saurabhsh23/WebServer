/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */
#pragma once

#include <condition_variable>
#include <iostream>
#include <queue>
#include <thread>
#include <vector>

#include "ThreadJob.hpp"

namespace Resources {

    class ThreadManager {
        std::vector<std::thread> pool{};

        std::mutex jobQueueMutex{};
        std::condition_variable condition{};

        std::queue<ThreadJob> jobs{};

        bool terminateFlag{false};
        const std::size_t maximumQueuedJobs;
    public:
        explicit
        ThreadManager(std::size_t threadCount, std::size_t maximumQueuedJobs)
                : maximumQueuedJobs(maximumQueuedJobs) {
            pool.reserve(threadCount);
            for (std::size_t i = 0; i < threadCount; i++) {
                pool.emplace_back([this](){ThreadEntrypoint();});
            }
        }

        ThreadManager(const ThreadManager &) = delete;
        ~ThreadManager() noexcept;

        /**
         * Wait until a thread slot is ready, if needed.
         */
        void
        WaitReady();

        void
        ThreadEntrypoint();

        inline void
        ScheduleJob(std::function<void()> &&entrypoint) {
            WaitReady();
            {
                std::unique_lock<std::mutex> lock(jobQueueMutex);
                jobs.push({this, entrypoint});
            }
            condition.notify_one();
        }
    };

} // namespace Resources
