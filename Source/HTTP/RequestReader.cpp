/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <iostream>
#include "RequestReader.hpp"

namespace HTTP {

    [[nodiscard]] constexpr std::string::size_type
    FindFirstTrailingWhitespace(const std::string &string) {
        for (std::string::size_type i = string.size() - 1; i >= 0; i--) {
            if (string[i] != '\t' && string[i] != ' ') {
                if (i == string.size() - 1) {
                    return std::string::npos;
                } else {
                    return i + 1;
                }
            }
        }

        return std::string::npos;
    }

    bool
    RequestReader::ConsumeHeader() {
        Header header{};
        char character = connection->ReadChar();
        if (character == '\r') {
            if (connection->ReadChar() == '\n') {
                // the end of the message has been reached.
                return false;
            }
            throw ReaderException {
                ErrorCode::INVALID_NEWLINE,
                DebugSection::HEADER_FIELD_NAME
            };
        }

        header.name = character;

        // Header name parsing
        while ((character = connection->ReadChar()) != ':') {
            ValidateHeaderFieldNameCharacter(character);
            header.name += character;
        }

        // OWS consuming
        while ((character = connection->ReadChar()) == ' ' || character == '\t') {
            // ignore the OWS between ":" and field-value
        }

        // Consume the header value
        header.value = character;
        while ((character = connection->ReadChar()) != '\r') {
            ValidateHeaderFieldValueCharacter(character);
            header.value += character;
        }

        // Consume the newline
        if (connection->ReadChar() != '\n') {
            throw ReaderException {
                ErrorCode::INVALID_NEWLINE,
                DebugSection::HEADER_FIELD_VALUE
            };
        }

        // Remove the trailing whitespace from the value
        const auto firstTrailingWhitespace = FindFirstTrailingWhitespace(header.value);
        if (firstTrailingWhitespace != std::string::npos) {
            header.value.resize(firstTrailingWhitespace);
        }

        request.headerList.data.push_back(std::move(header));

        return true;
    }

    HTTPVersion
    RequestReader::ConsumeHTTPVersion() {
        std::array<char, 8> buf{};
        connection->ReadBlock(buf);
        if (buf[0] != 'H' || buf[1] != 'T' || buf[2] != 'T' || buf[3] != 'P' || buf[4] != '/' || buf[6] != '.') {
            throw ReaderException {
                ErrorCode::HTTP_VERSION_INVALID_FORMAT,
                currentDebugSection
            };
        }

        if (buf[5] < '0' || buf[5] > '9' || buf[7] < '0' || buf[7] > '9') {
            throw ReaderException {
                    ErrorCode::HTTP_VERSION_INVALID_FORMAT,
                    currentDebugSection
            };
        }

        return HTTPVersion{
            static_cast<std::uint8_t>(buf[5] - '0'),
            static_cast<std::uint8_t>(buf[7] - '0')
        };
    }

    void
    RequestReader::ConsumeNewLine() {
        const char cr = connection->ReadChar();
        const char lf = connection->ReadChar();

        if (cr != '\r' || lf != '\n') {
            throw ReaderException {
                ErrorCode::INVALID_NEWLINE,
                currentDebugSection
            };
        }
    }

    std::string
    RequestReader::ConsumeVisibleCharactersUntilDelimiter(std::size_t maxLength, char delimiter) {
        std::string result{};

        while (true) {
            char character = connection->ReadChar();
            if (character == delimiter) {
                return result;
            }

            if (result.length() == maxLength) {
                throw ReaderException {
                        ErrorCode::STRING_TOO_LONG,
                        currentDebugSection
                };
            }

            if (!Characters::IsVChar(character)) {
                throw ReaderException {
                    ErrorCode::NON_VISIBLE_CHARACTER,
                    currentDebugSection
                };
            }

            result += character;
        }
    }

    std::string
    RequestReader::ConsumeRequestTarget(std::size_t maxLength) {
        return ConsumeVisibleCharactersUntilDelimiter(maxLength, ' ');
    }

    std::string
    RequestReader::ConsumeToken(std::size_t maxLength, char delimiter) {
        std::string result{};

        while (true) {
            char character = connection->ReadChar();
            if (character == delimiter) {
                return result;
            }

            if (Characters::IsTChar(character)) {
                if (result.length() == maxLength) {
                    throw ReaderException {
                            ErrorCode::STRING_TOO_LONG,
                            currentDebugSection,
                    };
                }

                result += character;
            } else {
                throw ReaderException {
                    ErrorCode::INVALID_CHARACTER_IN_TOKEN,
                    currentDebugSection
                };
            }
        }
    }

    void
    RequestReader::Read() {
        currentDebugSection = DebugSection::METHOD;
        request.method = ConsumeToken(limits.maximumMethodLength, ' ');

        currentDebugSection = DebugSection::REQUEST_TARGET;
        request.requestTarget = ConsumeRequestTarget(limits.maximumRequestTargetLength);

        currentDebugSection = DebugSection::HTTP_VERSION;
        request.version = ConsumeHTTPVersion();

        currentDebugSection = DebugSection::START_LINE_NEW_LINE;
        ConsumeNewLine();

        while (ConsumeHeader()) {
            // handling is done in the function itself.
        }
    }

    void
    RequestReader::ValidateHeaderFieldNameCharacter(char character) {
        if (!Characters::IsTChar(character)) {
            throw ReaderException {
                ErrorCode::INVALID_CHARACTER_IN_TOKEN,
                DebugSection::HEADER_FIELD_NAME
            };
        }
    }

    void
    RequestReader::ValidateHeaderFieldValueCharacter(char character) {
        if (!Characters::IsVChar(character) &&
                static_cast<std::uint8_t>(character) < 0x80 &&
                character != ' ' && character != '\t') {
            throw ReaderException {
                    ErrorCode::INVALID_CHARACTER_IN_HEADER_FIELD_VALUE,
                    DebugSection::HEADER_FIELD_NAME
            };
        }
    }

} // namespace HTTP
