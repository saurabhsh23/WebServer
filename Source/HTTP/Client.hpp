/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <utility>

#include "Source/Base/Configuration.hpp"
#include "Source/HTTP/Request.hpp"
#include "Source/HTTP/Response.hpp"
#include "Source/Resources/Connection.hpp"

namespace HTTP {

    enum class BadRequestReason {
        INVALID_REQUEST_TARGET,
    };

    class Client {
        const Base::Configuration &configuration;
        std::shared_ptr<Resources::Connection> connection;
        bool lastRequestWasMalformed{false};
        bool lastRequestConnectionClose{false};

    public:
        Client(const Base::Configuration &configuration,
               std::shared_ptr<Resources::Connection> connection)
                : configuration(configuration)
                , connection(std::move(connection)) {
        }

        void Run();

    private:

        [[nodiscard]] std::size_t
        CalculateResponseSize(const Response &) const;

        /**
         * Writes general information to the response.
         */
        void
        FinishResponse(Response &);

        void
        HandleRequest();

        [[nodiscard]] std::string
        ParseRequestTarget(const Request &) const;

        void
        SendBadRequest(const Request &, BadRequestReason);

        void
        SendResponse(const Request &, Response &);
    };

} // namespace HTTP
