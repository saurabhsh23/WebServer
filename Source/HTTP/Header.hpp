/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <string> // for std::string
#include <utility> // for std::move

namespace HTTP {

    /**
     * The structure for HTTP headers.
     *
     * https://svn.tools.ietf.org/svn/wg/httpbis/specs/rfc7230.html#header.fields
     */
    struct Header {

        std::string name;
        std::string value;

        Header() = default;

        Header(const std::string &name, const std::string &value)
            : name(name), value(value) {
        }

        Header(std::string &&name, std::string &&value)
            : name(std::move(name)), value(std::move(value)) {
        }

    };

} // namespace HTTP
