/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Header.hpp"

namespace HTTP {

    /**
     * The list of headers.
     */
    struct HeaderList {
    private:
        const Header emptyHeader{};
    public:
        std::vector<Header> data{};

        /**
         * Checks whether or not at least one field has a name matching the
         * given name.
         */
        [[nodiscard]] inline bool
        contains(const std::string_view &name) const noexcept {
            for (const Header &header : data) {
                if (header.name == name) {
                    return true;
                }
            }

            return false;
        }

        /**
         * Returns the field of the first header that matches the given name
         * with the field name.
         *
         * If no such header is found, an empty header is returned.
         *
         * When multiple headers exist with the same field name, the caller
         * can use the data attribute of this structure to iterate the content
         * of those headers.
         */
        [[nodiscard]] inline const Header &
        operator[](const std::string_view &name) const noexcept {
            for (const Header &header : data) {
                if (header.name == name) {
                    return header;
                }
            }

            return emptyHeader;
        }

    };

} // namespace HTTP
