/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Message.hpp"
#include "HeaderList.hpp"

namespace HTTP {

    /**
     * A request is an HTTP-message with the start name of a request line.
     *
     * https://svn.tools.ietf.org/svn/wg/httpbis/specs/rfc7230.html#request.line
     */
    struct Request : public Message {

        std::string method;
        std::string requestTarget;

        friend
        std::ostream &
        operator<<(std::ostream &output, const Request &request) {
            output << "Request(method=\"" << request.method
                   << "\", request-target: \"" << request.requestTarget
                   << "\")";
            return output;
        }

    };

} // namespace HTTP
