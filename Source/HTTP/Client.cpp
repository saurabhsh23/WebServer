/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Client.hpp"

#include "Source/HTTP/RequestReader.hpp"
#include "Source/Text/StringBuilder.hpp"

namespace HTTP {

    std::size_t
    Client::CalculateResponseSize(const Response &response) const {
        constexpr std::size_t httpVersionLength = 8;
        constexpr std::size_t colonLength = 1;
        constexpr std::size_t spaceLength = 1;
        constexpr std::size_t crlfLength = 2;

        const std::size_t startLineLength =
                httpVersionLength + spaceLength +
                response.statusCode.size() + spaceLength +
                response.reasonPhrase.size();

        std::size_t headersLength = 0;
        for (const Header &header : response.headerList.data) {
            headersLength +=
                    header.name.length() +
                    colonLength + spaceLength +
                    header.value.length() +
                    crlfLength;
        }

        return startLineLength + headersLength + crlfLength;
    }

    void
    Client::FinishResponse(Response &response) {
        response.version.major = 1;
        response.version.minor = 1;

        response.headerList.data.emplace_back("Server", "Wizard2");
    }

    void
    Client::HandleRequest() {
        RequestReader reader(connection, configuration.requestLimits);
        reader.Read();

        const Request &request = reader.GetResult();
        std::string path = ParseRequestTarget(request);
        if (path.empty()) {
            SendBadRequest(request, BadRequestReason::INVALID_REQUEST_TARGET);
        }

        Response response{};
        response.SetStatusCode<2, 0, 0>();
        response.reasonPhrase = "OK";
        response.SetMessageBody("Hello!");

        FinishResponse(response);

        SendResponse(request, response);
    }

    [[nodiscard]] std::string
    Client::ParseRequestTarget(const Request &request) const {
        if (request.requestTarget.empty()) {
            return "";
        }

        if (request.requestTarget.starts_with('/')) {
            return request.requestTarget;
        }

        return "";
    }

    void
    Client::Run() {
        while (!lastRequestConnectionClose && !lastRequestWasMalformed) {
            try {
                HandleRequest();
            } catch (...) {
                break;
            }
        }
    }

    void Client::SendBadRequest(const Request &request, BadRequestReason reason) {
        Response response{};
        response.SetStatusCode<4, 0, 0>();
        response.reasonPhrase = "Bad Request";

        switch (reason) {
            case BadRequestReason::INVALID_REQUEST_TARGET:
                response.SetMessageBody("Invalid Request Target");
                break;
        }

        FinishResponse(response);
        SendResponse(request, response);
    }

    void Client::SendResponse(const Request &request, Response &response) {
        static_cast<void>(request);

        // TODO determine other response methods (TE, content-encoding, etc.)
        response.headerList.data.emplace_back("Content-Length", std::to_string(response.messageBody.size()));

        Text::StringBuilder builder;
        builder.Reserve(CalculateResponseSize(response));

        builder.Append("HTTP/").Append('0' + response.version.major).Append('.').Append('0' + response.version.minor)
            .Append(' ')
            .Append(response.statusCode[0]).Append(response.statusCode[1]).Append(response.statusCode[2])
            .Append(' ')
            .Append(response.reasonPhrase)
            .Append("\r\n");

        for (const Header &header : response.headerList.data) {
            builder.Append(header.name).Append(": ").Append(header.value).Append("\r\n");
        }

        builder.Append("\r\n");

        connection->Write(builder);
        connection->Write(response.messageBody);
    }

} // namespace HTTP
