/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <string>

#include "Source/Base/Characters.hpp"
#include "Source/Base/Configuration.hpp"
#include "Source/HTTP/Request.hpp"
#include "Source/Resources/Connection.hpp"

namespace HTTP {

    /**
     * This class consumes and parses the request message.
     */
    class RequestReader {
    public:
        enum class ErrorCode {
            /** No error */
            NONE,

            /***
             * The given HTTP version isn't of format:
             * HTTP/x.y
             * Where x and y are numbers (0-9).
             */
            HTTP_VERSION_INVALID_FORMAT,

            /**
             * Token contains a character that is neither a tchar nor the
             * delimiter.
             */
            INVALID_CHARACTER_IN_TOKEN,

            /**
             * Header field value contains a character that isn't a vchar,
             * obs-text, or a space.
             */
            INVALID_CHARACTER_IN_HEADER_FIELD_VALUE,

            /**
             * Newlines in HTTP are always CRLF.
             */
            INVALID_NEWLINE,

            /**
             * ConsumeVisibleCharactersUntilDelimiter encountered a non-visible
             * character.
             */
            NON_VISIBLE_CHARACTER,

            /**
             * All input string have a given maximum length, and when the
             * string's length exceeds that maximum, this error code is used.
             */
            STRING_TOO_LONG,
        };

        enum class DebugSection {
            METHOD,
            REQUEST_TARGET,
            HTTP_VERSION,
            HEADER_FIELD_NAME,
            HEADER_FIELD_VALUE,
            MESSAGE_BODY,
            /**
             * The newline at the end of the startline
             */
            START_LINE_NEW_LINE,
        };

        [[nodiscard]] static inline constexpr const char *
        stringOfDebugSection(DebugSection debugSection) noexcept {
            switch (debugSection) {
                case DebugSection::HEADER_FIELD_NAME: return "HEADER_FIELD_NAME";
                case DebugSection::HEADER_FIELD_VALUE: return "HEADER_FIELD_VALUE";
                case DebugSection::HTTP_VERSION: return "HTTP_VERSION";
                case DebugSection::MESSAGE_BODY: return "MESSAGE_BODY";
                case DebugSection::METHOD: return "METHOD";
                case DebugSection::REQUEST_TARGET: return "REQUEST_TARGET";
                case DebugSection::START_LINE_NEW_LINE: return "START_LINE_NEW_LINE";
                default: return nullptr;
            }
        }

        [[nodiscard]] static inline constexpr const char *
        stringOfErrorCode(ErrorCode errorCode) noexcept {
            switch (errorCode) {
                case ErrorCode::HTTP_VERSION_INVALID_FORMAT: return "HTTP_VERSION_INVALID_FORMAT";
                case ErrorCode::INVALID_CHARACTER_IN_TOKEN: return "INVALID_CHARACTER_IN_TOKEN";
                case ErrorCode::INVALID_CHARACTER_IN_HEADER_FIELD_VALUE: return "INVALID_CHARACTER_IN_HEADER_FIELD_VALUE";
                case ErrorCode::INVALID_NEWLINE: return "INVALID_NEWLINE";
                case ErrorCode::NON_VISIBLE_CHARACTER: return "NON_VISIBLE_CHARACTER";
                case ErrorCode::NONE: return "NONE";
                case ErrorCode::STRING_TOO_LONG: return "STRING_TOO_LONG";
                default: return nullptr;
            }
        }

        friend std::ostream &
        operator<<(std::ostream &stream, ErrorCode errorCode) {
            stream << stringOfErrorCode(errorCode);
            return stream;
        }

        friend std::ostream &
        operator<<(std::ostream &stream, DebugSection debugSection) {
            stream << stringOfDebugSection(debugSection);
            return stream;
        }

        class ReaderException : public std::exception {
        public:
            ErrorCode errorCode;
            DebugSection debugSection;

            ReaderException(ErrorCode errorCode, DebugSection debugSection) noexcept
                : errorCode(errorCode), debugSection(debugSection)
            {}

            friend std::ostream &
            operator<<(std::ostream &stream, const ReaderException &exception) {
                stream << "ReaderException{ code: " << exception.errorCode << ", section: " << exception.debugSection << " }";
                return stream;
            }
        };

        RequestReader(const std::shared_ptr<Resources::Connection> &connection, const Base::Configuration::RequestLimits &limits)
            : connection(connection), limits(limits)
        {}

        /**
         * Reads the request line into the given Request.
         */
        void
        Read();

        [[nodiscard]] constexpr Request &
        GetResult() noexcept {
            return request;
        }

        [[nodiscard]] constexpr const Request &
        GetResult() const noexcept {
            return request;
        }

        /**
         * Returns true when an header was consumed, false if the header line
         * was the new line. The new line is the last line at the end of an
         * message, indicating the end of the message.
         */
        [[nodiscard]] bool
        ConsumeHeader();

        [[nodiscard]] HTTPVersion
        ConsumeHTTPVersion();

        void
        ConsumeNewLine();

        [[nodiscard]] std::string
        ConsumeRequestTarget(std::size_t maxLength);

        [[nodiscard]] std::string
        ConsumeToken(std::size_t maxLength, char delimiter);

        [[nodiscard]] std::string
        ConsumeVisibleCharactersUntilDelimiter(std::size_t maxLength, char delimiter);

        void
        ValidateHeaderFieldNameCharacter(char);

        void
        ValidateHeaderFieldValueCharacter(char);

    private:
        std::shared_ptr<Resources::Connection> connection;
        const Base::Configuration::RequestLimits &limits;
        Request request{};
        DebugSection currentDebugSection{};

    };

} // namespace HTTP
