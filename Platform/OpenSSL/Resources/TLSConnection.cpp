/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <openssl/err.h>

#include <iostream>

namespace Resources {

    TLSConnection::~TLSConnection() noexcept {
        SSL_shutdown(data);
        SSL_free(data);
        Resources::SocketSSLGlue::DestroySocketInfo(socketInfo);
    }

    char
    TLSConnection::ReadChar() {
        char character;

        if (SSL_read(data, &character, 1) != 1) {
            throw TLSConnectionException {
                "[OpenSSL] Failed ReadChar"
            };
        }

        return character;
    }

    void
    TLSConnection::ReadBlock(char *outData, std::size_t length) {
        do {
            int ret = SSL_read(data, outData, length);

            if (ret <= 0) {
                // ERR_print_errors_fp(stdout);
                throw TLSConnectionException {
                        "[OpenSSL] Failed ReadBlock"
                };
            }

            outData += ret;
            length -= ret;
        } while (length != 0);
    }

    void
    TLSConnection::Write(const char *srcData, std::size_t length) {
        do {
            int ret = SSL_write(data, srcData, length);

            if (ret <= 0) {
                // ERR_print_errors_fp(stdout);
                throw TLSConnectionException {
                        "[OpenSSL] Failed Write"
                };
            }

            srcData += ret;
            length -= ret;
        } while (length != 0);
    }

} // namespace Resources
