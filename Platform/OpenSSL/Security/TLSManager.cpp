/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <openssl/err.h>

#include "Source/Resources/TLSConnection.hpp"

namespace Security {

    TLSManager::TLSManager(const Base::Configuration &configuration) {
        context = SSL_CTX_new(SSLv23_method());

        if (!context) {
            throw TLSManagerException{"[OpenSSL] Failed to create context!"};
        }

        if (SSL_CTX_use_certificate_file(context,
                                         configuration.tlsConfiguration.certificateFile.c_str(),
                                         SSL_FILETYPE_PEM
                ) != 1) {
            ERR_print_errors_fp(stderr);
            throw TLSManagerException{"[OpenSSL] Failed to load certificate file!"};
        }

        if (SSL_CTX_use_PrivateKey_file(context, configuration.tlsConfiguration.privateKeyFile.c_str(), SSL_FILETYPE_PEM) <= 0) {
            ERR_print_errors_fp(stderr);
            throw TLSManagerException{"[OpenSSL] Failed to load private-key file!"};
        }

        FILE *file = std::fopen(configuration.tlsConfiguration.certificateChainFile.c_str(), "r");

        if (!file) {
            throw TLSManagerException{"[OpenSSL] Failed to load chain file!"};
        }

        while (X509 *cert = PEM_read_X509(file, nullptr, nullptr, nullptr)) {
            if (!SSL_CTX_add_extra_chain_cert(context, cert)) {
                ERR_print_errors_fp(stderr);
                fclose(file);
                X509_free(cert);
                throw TLSManagerException{"[OpenSSL] Failed to add extra chain certificate!"};
            }
        }

        // Clear errors from PEM_read_X509 since the error stack probably contains
        // an error regarding an EOF in the chain file.
        ERR_clear_error();

        std::fclose(file);

        SSL_CTX_set_min_proto_version(context, TLS1_2_VERSION);
    }

    TLSManager::~TLSManager() noexcept {
        if (context) {
            SSL_CTX_free(context);
        }

        EVP_cleanup();
    }

    std::shared_ptr<Resources::Connection>
    TLSManager::CreateTLSWrappedConnection(
            Resources::SocketSSLGlue::SocketType socketInfo) {
        SSL *ssl = SSL_new(context);
        if (!ssl) {
            ERR_print_errors_fp(stderr);
            throw TLSManagerException{"[OpenSSL] Failed to SSL_new!"};
        }

        if constexpr (std::is_same_v<Resources::SocketSSLGlue::SocketType, int>) {
            if (SSL_set_fd(ssl, socketInfo) == 0) {
                throw TLSManagerException{"[OpenSSL] Failed to initialize fd to SSL*!"};
            }
        }

        int status = SSL_accept(ssl);
        if (status != 1) {
            ERR_print_errors_fp(stderr);
            throw TLSManagerException{"[OpenSSL] Failed SSL_accept"};
        }

        status = SSL_do_handshake(ssl);
        if (status != 1) {
            throw TLSManagerException{"[OpenSSL] Failed SSL_do_handshake"};
        }

        return std::make_shared<Resources::TLSConnection>(ssl, socketInfo);
    }

} // namespace Security
