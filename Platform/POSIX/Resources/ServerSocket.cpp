/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * This file is included by Source/Resources/ServerSocket.cpp
 */

#include <cerrno>
#include <cstring>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <array>
#include <sstream>

#if !defined(AF_INET6) || defined(DONT_USE_IPV6)
#define FORCE_IPV4
#endif

#include "Source/Resources/SocketConnection.hpp"

namespace Resources {

    [[nodiscard]] static std::string
    FormatGenericError(int errnum) {
        std::array<char, 2048> messageLog{};
        strerror_r(errnum, messageLog.data(), messageLog.size());
        return std::string(std::begin(messageLog), std::end(messageLog));
    }

    [[nodiscard]] static std::string
    FormatBindError(int errnum, int port) {
        std::stringstream stream{};

        stream << "Failed to bind to port: " << port << ": ";

        switch (errnum) {
            case EACCES:
                stream << "insufficient permissions";
                break;
            case EADDRINUSE:
                stream << "port in use";
                break;
            default: {
                std::array<char, 2048> messageLog{};
                strerror_r(errnum, messageLog.data(), messageLog.size());
                stream << "unknown error: '" << messageLog.data() << "'!";
                break;
            }
        }

        return stream.str();
    }

    [[nodiscard]] static std::string
    FormatListenError(int errnum, int port) {
        std::stringstream stream{};

        stream << "Failed to listen on port: " << port << ": ";

        if (errnum == EADDRINUSE) {
            stream << "port is already in use";
        } else {
            std::array<char, 2048> messageLog{};
            strerror_r(errnum, messageLog.data(), messageLog.size());
            stream << "unknown error: '" << messageLog.data() << "'!";
        }

        return stream.str();
    }

    [[nodiscard]] static int
    CreateSocket() {
#ifdef FORCE_IPV4
        int sockfd = socket(AF_INET, SOCK_STREAM, 0);
#else
        int sockfd = socket(AF_INET6, SOCK_STREAM, 0);
#endif
        if (sockfd == -1) {
            throw ServerSocketException {
                "Failed to create an UNIX socket!"
            };
        }

        return sockfd;
    }

    static void
    BindSocket(int sockfd, std::uint16_t port) {
#ifdef FORCE_IPV4
        struct sockaddr_in address{};
        address.sin_family = AF_INET;
        address.sin_port = htons(port);
        address.sin_addr.s_addr = htonl(INADDR_ANY);
#else
        struct sockaddr_in6 address{};
        address.sin6_family = AF_INET6;
        address.sin6_port = htons(port);
        address.sin6_addr = IN6ADDR_ANY_INIT;
#endif

        if (bind(sockfd, reinterpret_cast<struct sockaddr *>(&address), sizeof(address)) == -1) {
            throw ServerSocketException {
                FormatBindError(errno, port)
            };
        }
    }

    static void
    ConfigureSocket(int sockfd, std::uint16_t port) {
        static_cast<void>(port);

        int reuseOption = 1;

        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuseOption, sizeof(reuseOption)) < 0) {
            throw ServerSocketException {
                "Failed to set the UNIX SO_REUSEADDR flag: " + FormatGenericError(errno)
            };
        }
    }

    static void
    ListenSocket(int sockfd, int port, int listenerBacklog) {
        if (listen(sockfd, listenerBacklog) == -1) {
            throw ServerSocketException {
                    FormatListenError(errno, port)
            };
        }
    }

    ServerSocket::ServerSocket(std::uint16_t port, int listenerBacklog) {
        sockfd = CreateSocket();
        ConfigureSocket(sockfd, port);
        BindSocket(sockfd, port);
        ListenSocket(sockfd, port, listenerBacklog);
    }

    ServerSocket::~ServerSocket() noexcept {
        Shutdown();
    }

    SocketSSLGlue::SocketType
    ServerSocket::AcceptSocket() {
        start:
        int client = accept(sockfd, nullptr, nullptr);

        if (client != -1) {
            return client;
        }

        switch (errno) {
            case EAGAIN:
#if EAGAIN != EWOULDBLOCK
                case EWOULDBLOCK:
#endif
            case ECONNABORTED:
                // TODO make non-blocking support better and don't use goto
                goto start;
            case EBADF:
                if (sockfd == -1) {
                    throw ServerSocketException{
                            "Socket is closed explicitly",
                            true
                    };
                }
                [[fallthrough]];
            default:
                // other errors are caused by the OS closing the server socket,
                // or the syscall indicating the call is malformed in another
                // way, thus declaring it server-level fatal.
                throw ServerSocketException {
                        FormatGenericError(errno),
                        true
                };
        }
    }

    std::shared_ptr<Connection>
    ServerSocket::Accept() {
        return std::make_shared<SocketConnection>(AcceptSocket());
    }

    std::shared_ptr<Connection>
    ServerSocket::AcceptTLS(Security::TLSManager *manager) {
        return manager->CreateTLSWrappedConnection(AcceptSocket());
    }

    void
    ServerSocket::Shutdown() noexcept {
        if (sockfd != -1) {
            int fd = sockfd;
            sockfd = -1;
            close(fd);
        }
    }

}
