/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Testing/TestSymbols.hpp"

#include "Base/Characters.hpp"

inline void
IterateASCII(const auto &function) {
    for (std::size_t i = 0; i < 0x100; i++) {
        function(i);
    }
}

TEST(Base_Characters, IsVChar) {
    IterateASCII([](std::size_t i) {
        if (i < 0x21 || i > 0x7E) {
            EXPECT_FALSE(Characters::IsVChar(i));
        } else {
            EXPECT_TRUE(Characters::IsVChar(i));
        }
    });
}

TEST(Base_Characters, IsDelimiter) {
    // TODO Add test for Characters::IsDelimiter
}

TEST(Base_Characters, IsTChar) {
    IterateASCII([](char c) {
        bool isAlpha = (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
        bool isDigit = (c >= '0' && c <= '9');
        bool isOther = c == '!' || c == '#'  || c == '$' || c == '%' || c == '&'
                                || c == '\'' || c == '*' || c == '+' || c == '-'
                                || c == '.'  || c == '^' || c == '_' || c == '`'
                                || c == '|'  || c == '~';

        if (isAlpha || isDigit || isOther) {
            EXPECT_TRUE(Characters::IsTChar(c));
        } else {
            EXPECT_FALSE(Characters::IsTChar(c));
        }
    });
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
