/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <string_view>

#include "Testing/TestSymbols.hpp"
#include "Source/Resources/MockConnection.hpp"

#include "Source/HTTP/RequestReader.hpp"

constexpr Security::RequestLimits requestLimits{};
std::uniform_int_distribution<char8_t> asciiDistribution(std::numeric_limits<char8_t>::min(), std::numeric_limits<char8_t>::min() + 0x80);
std::uniform_int_distribution<char8_t> visibleDistribution(0x21, 0x7E);

template<std::size_t InputLength>
[[nodiscard]] inline std::shared_ptr<Resources::MockConnection>
CreateConnection(const char (&input)[InputLength]) {
    return std::make_shared<Resources::MockConnection>(
            std::vector<char>(std::cbegin(input), std::cend(input) - 1)
    );
}

[[nodiscard]] inline std::shared_ptr<Resources::MockConnection>
CreateConnection(std::string_view sv) {
    return std::make_shared<Resources::MockConnection>(
            std::vector<char>(std::cbegin(sv), std::cend(sv))
    );
}

TEST(HTTP_RequestReader, SimpleTest) {
    auto connection = CreateConnection("GET / HTTP/1.1\r\nDate: invalid\r\n\r\n");

    HTTP::RequestReader reader(connection, requestLimits);
    reader.Read();

    EXPECT_EQ(reader.GetResult().method, "GET"sv);
    EXPECT_EQ(reader.GetResult().requestTarget, "/"sv);
    EXPECT_EQ(reader.GetResult().version.major, 1);
    EXPECT_EQ(reader.GetResult().version.minor, 1);

    ASSERT_EQ(reader.GetResult().headerList.data.size(), 1);
    EXPECT_EQ(reader.GetResult().headerList.data[0].name, "Date");
    EXPECT_EQ(reader.GetResult().headerList.data[0].value, "invalid");
}

TEST(HTTP_RequestReader, InvalidMethodTest) {
    std::string requestTemplate{"G T / HTTP/1.1\r\n\r\n"};

    for (std::size_t i = 0; i < 0x100; i++) {
        char character = static_cast<char>(i);
        if (Characters::IsDelimiter(character)) {
            std::string request = requestTemplate;
            request[1] = character;

            auto connection = CreateConnection(request);
            HTTP::RequestReader reader(connection, requestLimits);

            try {
                reader.Read();

                FAIL() << "Reader accepted method: " << std::string_view(request.c_str(), 3);
            } catch (const HTTP::RequestReader::ReaderException &exception) {
                EXPECT_EQ(exception.errorCode, HTTP::RequestReader::ErrorCode::INVALID_CHARACTER_IN_TOKEN);
            }
        }
    }
}

TEST(HTTP_RequestReader, ValidMethodTest) {
    std::string requestTemplate{"G T / HTTP/1.1\r\n\r\n"};

    for (std::size_t i = 0; i < 0x100; i++) {
        char character = static_cast<char>(i);
        if (Characters::IsTChar(character)) {
            std::string request = requestTemplate;
            const std::string_view method(request.c_str(), 3);

            request[1] = character;

            auto connection = CreateConnection(request);
            HTTP::RequestReader reader(connection, requestLimits);

            try {
                reader.Read();
                EXPECT_EQ(reader.GetResult().method, method);
                EXPECT_EQ(reader.GetResult().requestTarget, "/");
            } catch (const HTTP::RequestReader::ReaderException &) {
                FAIL() << "Reader rejected method: " << method;
            }
        }
    }
}

/**
 * The reader should accept any characters, except for non-ASCII characters and
 * the delimiter U+0020 SPACE.
 *
 * The validation and parsing is not done by the reader.
 */
TEST(HTTP_RequestReader, RequestTargetTest) {
    std::string requestPrefix{"GET "};
    std::string requestSuffix{" HTTP/1.1\r\n\r\n"};

    for (std::size_t i = 0; i < RANDOM_SAMPLES; i++) {
        const auto targetLength = sizeDistribution(randomDevice);
        std::string requestTarget{};
        for (std::size_t j = 0; j < targetLength; j++) {
            char character = visibleDistribution(randomDevice);
            if (character != ' ') {
                requestTarget += character;
            }
        }

        std::string request = requestPrefix;
        request += requestTarget;
        request += requestSuffix;
        auto connection = CreateConnection(request);
        HTTP::RequestReader reader(connection, requestLimits);

        try {
            reader.Read();
            EXPECT_EQ(reader.GetResult().method, "GET"sv);
            EXPECT_EQ(reader.GetResult().requestTarget, requestTarget);
        } catch (const HTTP::RequestReader::ReaderException &exception) {
            FAIL() << "Reader rejected request-target: " << requestTarget << " " << exception;
        }
    }
}

TEST(HTTP_RequestReader, ValidHTTPVersionTest) {
    for (char major = '0'; major <= '9'; major++) {
        for (char minor = '0'; minor <= '9'; minor++) {
            std::string request = "GET / HTTP/";
            request += major;
            request += '.';
            request += minor;
            request += "\r\n\r\n";
            auto connection = CreateConnection(request);
            HTTP::RequestReader reader(connection, requestLimits);

            try {
                reader.Read();
                EXPECT_EQ(reader.GetResult().method, "GET");
                EXPECT_EQ(reader.GetResult().requestTarget, "/");
                EXPECT_EQ(reader.GetResult().version.major, major - '0');
                EXPECT_EQ(reader.GetResult().version.minor, minor - '0');
            } catch (const HTTP::RequestReader::ReaderException &exception) {
                FAIL() << "Reader rejected HTTP version: " << (major+'0') << '.' << (minor+'0') << ": " << exception;
            }
        }
    }
}

TEST(HTTP_RequestReader, HeaderValueTrailingWhitespaceRemoval) {
    constexpr std::array inputs = {
            "GET / HTTP/1.1\r\nTest-Name: \t     test-value   \t  \r\n\r\n",
            "GET / HTTP/1.1\r\nTest-Name:test-value\r\n\r\n",
            "GET / HTTP/1.1\r\nTest-Name:\ttest-value\r\n\r\n",
            "GET / HTTP/1.1\r\nTest-Name:\ttest-value\t\r\n\r\n",
            "GET / HTTP/1.1\r\nTest-Name: test-value\t\r\n\r\n",
            "GET / HTTP/1.1\r\nTest-Name:     test-value      \r\n\r\n",
            "GET / HTTP/1.1\r\nTest-Name:\t     test-value      \t\r\n\r\n",
    };

    std::size_t i = 0;
    for (const auto &input : inputs) {
        auto connection = CreateConnection(input);
        HTTP::RequestReader reader(connection, requestLimits);
        try {
            reader.Read();
            EXPECT_EQ(reader.GetResult().method, "GET");
            EXPECT_EQ(reader.GetResult().requestTarget, "/");
            EXPECT_EQ(reader.GetResult().version.major, 1);
            EXPECT_EQ(reader.GetResult().version.minor, 1);
            EXPECT_EQ(reader.GetResult().headerList.data.size(), 1);
            EXPECT_EQ(reader.GetResult().headerList.data[0].name, "Test-Name");
            EXPECT_EQ(reader.GetResult().headerList.data[0].value, "test-value") << "Rejected input: \n======================================\n"
                                                                                    << input
                                                                                    << "\n====================================== is " << i;
            i++;
        } catch (const HTTP::RequestReader::ReaderException &exception) {
            FAIL() << "Rejected input: \n======================================\n"
                   << input
                   << "\n======================================\nWith exception: "
                   << exception;
        }
    }

}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
